package facci.pm.cedenoalvia.examen_retrofit;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JSONPlaceholder {

    @GET("albums")
    Call<List<Albums>>getAlbums();

}
