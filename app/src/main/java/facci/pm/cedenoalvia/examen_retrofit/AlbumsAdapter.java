package facci.pm.cedenoalvia.examen_retrofit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.AlbumsViewHolder> {

    List<Albums> albumsList;
    Context context;

    public AlbumsAdapter(Context context, List<Albums> albums){
        this.context = context;
        albumsList = albums;
    }

    @NonNull
    @Override
    public AlbumsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        return new AlbumsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumsViewHolder holder, int position) {

        Albums albums = albumsList.get(position);
        holder.title.setText(albums.getTitle());
        holder.id.setText(albums.getId());
        holder.userid.setText(albums.getUserId());

    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    public class AlbumsViewHolder extends RecyclerView.ViewHolder{

        TextView title, id, userid;

        public AlbumsViewHolder(@NonNull View itemView){
            super(itemView);

            title = itemView.findViewById(R.id.title_album);
            id = itemView.findViewById(R.id.id_album);
            userid = itemView.findViewById(R.id.userid_album);

        }

    }
}
